#!/bin/bash
#!!!in DEVEL!!!
# First shell for samba-container

. shell-config

PATH_TO_CONFIG='../etc/samba/samba_docker.conf'
#PATH_TO_UTILS='../utilities/'
#source $PATH_TO_UTILS/status.sh
SMB_D_CONTAINER_NAME="$(shell_config_get "$PATH_TO_CONFIG" "SMB_D_CONTAINER_NAME" "=")"
SMB_D_CONTAINER_NAME="${SMB_D_CONTAINER_NAME//[\"\'\\]}"

function _help() {
    echo "Try to use:
    create     - to download samba image and prepare to start samba-core in container.
    provision  - to create a domain controller with container core.
    start      - to start samba core in the container with created environment.
    delete     - to delete image and/or container."
}

function _check_samba_service() {
  local SAMBA_STATUS
  if [ -f "/usr/bin/systemctl" ]; then
    SAMBA_STATUS=$(/usr/bin/systemctl is-active samba.service)
  elif [ -f "/sbin/service" ]; then
    SAMBA_STATUS=$(/sbin/service samba status)
  fi
  echo "$SAMBA_STATUS"
}

#TODO: think about preparing directories
function _create_dir_form_config() {
    local DIR
    if [[ -n "$1" ]]; then
      DIR=$1
    else
      return 0
    fi
    while true; do
        read -rp "Create directory $DIR?
[Yes or No]:" yn
        case $yn in
            [Yy]* )
            mkdir -p "$DIR"
            chown root:root "$DIR"
            chmod  755 "$DIR"
    	      return 0
    	      ;;
            [Nn]* ) return 1 ;;
            * ) echo "Please enter yes or no";;
        esac
    done
}

#TODO: think about criteria
function _check_samba_files() {
  local SMB_D_CONFIG_DIR
  local SMB_D_LIBRARIES_DIR
  local SMB_D_CACHE_DIR
  local STATUS
SMB_D_CONFIG_DIR="$(shell_config_get "$PATH_TO_CONFIG" "SMB_D_CONFIG_DIR" "=")"
SMB_D_CONFIG_DIR=${SMB_D_CONFIG_DIR//[\"\'\\]}
SMB_D_LIBRARIES_DIR="$(shell_config_get "$PATH_TO_CONFIG" "SMB_D_LIBRARIES_DIR" "=")"
SMB_D_LIBRARIES_DIR=${SMB_D_LIBRARIES_DIR//[\"\'\\]}
SMB_D_CACHE_DIR="$(shell_config_get "$PATH_TO_CONFIG" "SMB_D_CACHE_DIR" "=")"
SMB_D_CACHE_DIR=${SMB_D_CACHE_DIR//[\"\'\\]}
if [[ -n $SMB_D_CONFIG_DIR ]] && [[ ! -d $SMB_D_CONFIG_DIR ]]; then
    _create_dir_form_config "$SMB_D_CONFIG_DIR"
fi
if [[ -n $SMB_D_LIBRARIES_DIR ]] && [[ ! -d $SMB_D_LIBRARIES_DIR ]]; then
    _create_dir_form_config "$SMB_D_LIBRARIES_DIR"
    mkdir -p "$SMB_D_LIBRARIES_DIR/sysvol" && chown root:root "$SMB_D_LIBRARIES_DIR/sysvol" && chmod  755 "$SMB_D_LIBRARIES_DIR/sysvol"
elif [[ -n $SMB_D_LIBRARIES_DIR ]] && [[ "$(find "$SMB_D_LIBRARIES_DIR/" -maxdepth 1 -type f -iname '*db' | wc -l)" -gt 0  ]]; then
    STATUS="not_empty"
fi
if [[ -n $SMB_D_CACHE_DIR ]] && [[ ! -d $SMB_D_CACHE_DIR ]]; then
    _create_dir_form_config "$SMB_D_CACHE_DIR"
elif [[ -n $SMB_D_CACHE_DIR ]] && [[ "$(find "$SMB_D_CACHE_DIR/" -maxdepth 1 -type f | wc -l)" -gt 0  ]]; then
    STATUS="not_empty"
fi
echo $STATUS
}

function _check_status() {
  local CONTAINER_NAME=$1
  CONTAINER_STATUS=$(docker inspect -f '{{.State.Status}}' "$CONTAINER_NAME" 2>/dev/null)
  if [[ "$CONTAINER_STATUS" =~ "running" ]]; then
    echo "running"
  elif [[ "$CONTAINER_STATUS" =~ "exited" ]] || [[ $CONTAINER_STATUS =~ "paused" ]]; then
    echo "stay"
  elif [ -z "$CONTAINER_STATUS" ]; then
    echo "missing"
  else
    echo "Something wrong"
  fi
}

function _pull_samba_img() {
  local REGISTRY="$1"
  local KRB_AUTH_TYPE="$2"
  local IMG_VERSION="$3"
  echo "Downloading samba core image."
  docker image pull "$REGISTRY$KRB_AUTH_TYPE:$IMG_VERSION"
  docker image tag "$REGISTRY$KRB_AUTH_TYPE:$IMG_VERSION" "downloaded/basealt/samba-core-dc:local"
}

function _start_samba_container() {
#TODO: think about autostart key
  local SAMBA_STATUS
  local CONTAINER_STATUS
  local SMB_D_CONFIG_DIR
  local SMB_D_LIBRARIES_DIR
  local SMB_D_CACHE_DIR

  echo "Start samba core in container."
#Stop host samba service if exists
    SAMBA_STATUS=$(_check_samba_service)

    if [ "$SAMBA_STATUS" == "active" ] || [ "$SAMBA_STATUS" == "samba is running" ]; then
      if [ -f "/usr/bin/systemctl" ]; then
        /usr/bin/systemctl stop samba.service
      else
        /sbin/service samba stop
      fi
      else
        echo "Samba service was not found."
    fi
#Check container status if it exists, start it
    CONTAINER_STATUS="$(_check_status "$SMB_D_CONTAINER_NAME")"

    if [ "$CONTAINER_STATUS" == "stay" ]; then
      docker start "$SMB_D_CONTAINER_NAME"
    elif [ "$CONTAINER_STATUS" == "running" ]; then
      echo "Container $CONTAINER_NAME is already running"
    elif [[ "$CONTAINER_STATUS" == "missing" ]]; then
      if [ "$(docker inspect --format='{{.Id}}' "downloaded/basealt/samba-core-dc:local" 2>/dev/null)" ]; then
       #TODO think about hostname
        SMB_D_CONFIG_DIR="$(shell_config_get "$PATH_TO_CONFIG" "SMB_D_CONFIG_DIR" "=")"
        SMB_D_CONFIG_DIR=${SMB_D_CONFIG_DIR//[\"\'\\]}
        SMB_D_LIBRARIES_DIR="$(shell_config_get "$PATH_TO_CONFIG" "SMB_D_LIBRARIES_DIR" "=")"
        SMB_D_LIBRARIES_DIR=${SMB_D_LIBRARIES_DIR//[\"\'\\]}
        SMB_D_CACHE_DIR="$(shell_config_get "$PATH_TO_CONFIG" "SMB_D_CACHE_DIR" "=")"
        SMB_D_CACHE_DIR=${SMB_D_CACHE_DIR//[\"\'\\]}
        if [[ "$2" != "provision" ]]; then
          docker run -d --cap-add=SYS_ADMIN --net=host --name="$SMB_D_CONTAINER_NAME" \
          -v "$SMB_D_CONFIG_DIR":/etc/samba/ \
          -v "$SMB_D_LIBRARIES_DIR":/var/lib/samba \
          -v "$SMB_D_CACHE_DIR":/var/cache/samba \
          "downloaded/basealt/samba-core-dc:local"
        elif [[ "$2" == "provision" ]]; then
          shift; shift
          docker run -d --net=host --cap-add=SYS_ADMIN --name="$SMB_D_CONTAINER_NAME" \
            -v "$SMB_D_CONFIG_DIR":/etc/samba/ \
            -v "$SMB_D_LIBRARIES_DIR":/var/lib/samba \
            -v "$SMB_D_CACHE_DIR":/var/cache/samba \
            -it "downloaded/basealt/samba-core-dc:local" /bin/bash
          docker exec -it "$SMB_D_CONTAINER_NAME" sh -c "/usr/bin/samba-tool domain provision $* ; killall /bin/bash"
          docker rm "$SMB_D_CONTAINER_NAME"
#          echo "Container ready now. Please use 'samba-container start'"
        fi
      else
        echo "Download samba core image before $2. Use 'samba-container create'"
      fi
    fi
}

if [ -z "$1" ]; then
  _help
  exit
fi

case "$1" in
  create) shift
  #Download image with parameters
    while [ -n "$1" ]; do
      case "$1" in
        --registry=*)
          REGISTRY="${1#*=}"
          if [[ -n "$REGISTRY" ]]; then
              echo "Using registry $REGISTRY"
          fi
          shift
        ;;
        --krb-auth-type=*)
          KRB_AUTH_TYPE="${1#*=}"
          # Set image by type
          if   [[ -n "$KRB_AUTH_TYPE" ]] && [[ "$KRB_AUTH_TYPE" =~ ^(MIT|mit|mitkrb5)$ ]]; then KRB_AUTH_TYPE='samba-dc-mitkrb5';
          elif [[ -n "$KRB_AUTH_TYPE" ]] && [[ "$KRB_AUTH_TYPE" =~ ^(HEIMDAL|heimdal)$ ]]; then KRB_AUTH_TYPE='samba-dc';
          else echo "Unknown authentication method. Choose heimdal or mit methods
          or don't set the '--krb-auth-type' key to use the default value - heimdal
          " ; fi
          shift
        ;;
        --version-img=*)
          IMG_VERSION="${1#*=}"
          if [[ -n "$IMG_VERSION" ]]; then echo "Trying to download a version/tag $IMG_VERSION image";
          else echo "Unrecognized version looks like the version unset.
          Set version or don't set the '--version-img' key to use last version"
          fi
          shift
        ;;
        -h|--help|*) echo "Choose which image you needed
  --krb-auth-type={heimdal|mit}
      heimdal  - image with heimdal authentication method.
      mit      - image with MIT authentication method.
  --version-img={version}
      Set registry version if it's needed.
  --registry={registry_name}
      Set registry name if it's needed."
      HELP=1
          shift
        ;;
      esac
    done
#Set default from config file
    if [ -z "$HELP" ]; then
      if  [ -z "$REGISTRY" ]; then
        REGISTRY="$(shell_config_get "$PATH_TO_CONFIG" "SMB_D_REGISTRY" "=")"
        REGISTRY="${REGISTRY//[\"\'\\]}"
        echo "- Registry is not set. The default value is taken from config file. Default is $REGISTRY."
      fi
      if [ -z "$KRB_AUTH_TYPE" ]; then
        KRB_AUTH_TYPE="$(shell_config_get "$PATH_TO_CONFIG" "SMB_D_IMAGE" "=")"
        KRB_AUTH_TYPE="${KRB_AUTH_TYPE//[\"\'\\]}"
        echo "- Samba authentication type is not set. The default value is taken from config file. Default is kerberos with heimdal."
      fi
      [ -z "$IMG_VERSION" ] && IMG_VERSION='latest' \
       && echo "- Image version is not set. Trying to get the latest version."
#Execute result
        _pull_samba_img "$REGISTRY" "$KRB_AUTH_TYPE" "$IMG_VERSION"
    fi
  ;;
  provision)
    echo "Start provision: "
    CONTAINER_STATUS="$(_check_status "$SMB_D_CONTAINER_NAME")"
    if [[  $(_check_samba_files) == "not_empty" ]]; then
      echo "  Failed: Looks like the container $SMB_D_CONTAINER_NAME already exists.
  Please clear samba-dc work directories before provision or change it with $PATH_TO_CONFIG."
      exit
    elif [[ $CONTAINER_STATUS != "missing" ]]; then
      echo "  Failed: Samba work directories not empty.
  Try to delete it 'samba-container delete'
  or maybe you just need to start an existing samba-dc core 'samba-container start'"
      exit
    fi
    shift
    _start_samba_container "$SMB_D_CONTAINER_NAME" provision "$*"
    #samba-tool domain provision --realm=domain.alt --domain domain --adminpass='Pa$$word' --dns-backend=SAMBA_INTERNAL --server-role=dc
    #echo "exec $(shell_config_get "$PATH_TO_CONFIG" "SMB_D_CONTAINER_NAME" "=")"" sh -c \"/usr/bin/samba-tool domain provision $*\""
    #docker container inspect alt_samba_dc_2 > /dev/null 2>&1 || echo good
    #docker exec "$SMB_D_CONTAINER_NAME" sh -c "/usr/bin/samba-tool domain provision $*";
    #docker run --entrypoint '/bin/bash' --net=host --cap-add=SYS_ADMIN --name="$SMB_D_CONTAINER_NAME" \
     #"samba-tool domain provision --realm=domain.alt --domain domain --adminpass='Pa\$\$Wd!C##' --dns-backend=SAMBA_INTERNAL --server-role=dc --hostname=DC.DOMAIN.ALT; /bin/bash"
  ;;
  start)
    SMB_D_LIBRARIES_DIR="$(shell_config_get "$PATH_TO_CONFIG" "SMB_D_LIBRARIES_DIR" "=")"
    SMB_D_LIBRARIES_DIR=${SMB_D_LIBRARIES_DIR//[\"\'\\]}
    if [[ -d $SMB_D_LIBRARIES_DIR ]] && [[ "$(find "$SMB_D_LIBRARIES_DIR/" -maxdepth 1 -type f -iname '*db' | wc -l)" -gt 0  ]]; then
      _start_samba_container "$SMB_D_CONTAINER_NAME"
    else
     echo "Looks like samba hasn't been provisioned. Try using 'provision' key to provision samba"
    fi
  ;;
  delete)
    CONTAINER_STATUS="$(_check_status "$SMB_D_CONTAINER_NAME")"
    [ "$CONTAINER_STATUS" == "running" ] && docker stop  "$SMB_D_CONTAINER_NAME"
     docker rm  "$SMB_D_CONTAINER_NAME"
     docker image rm  -f "$(docker inspect  --format='{{.Id}}' "downloaded/basealt/samba-core-dc:local" 2>/dev/null)"
  ;;
  *) echo -e "$1 it's not correct option"
    _help
  ;;
esac
