#!/bin/bash

source ./samba_docker.conf

#CONTAINER_STATUS=$(docker container inspect $CONTAINER_NAME | grep Status | awk -e '{print $2}')
#CONTAINER_STATUS=$(docker container inspect $CONTAINER_NAME | jq .[].State.Status)
CONTAINER_STATUS=$(docker inspect -f '{{.State.Status}}' "$SMB_D_CONTAINER_NAME" 2>/dev/null)

function check_status() {
  if [[ "$1" =~ "running" ]]; then
    echo "it's running"
  elif [[ "$1" =~ "exited" ]] || [[ $1 =~ "paused" ]]; then
    docker start "$SMB_D_CONTAINER_NAME"
  else
    check_status "$1"
  fi
}

if [[ -n $CONTAINER_STATUS ]]; then
  echo "$CONTAINER_STATUS"
  check_status "$CONTAINER_STATUS"
else
  #TODO think about hostname
    docker run -it --hostname=dc.domain.alt --cap-add=SYS_ADMIN --net=host --name="$SMB_D_CONTAINER_NAME" \
    -v "$SMB_D_CONFIG_DIR":/etc/samba/ \
    -v "$SMB_D_LIBRARIES_DIR":/var/lib/samba \
    -v "$SMB_D_CACHE_DIR":/var/cache/samba \
    "$SMB_D_REGISTRY$SMB_D_IMAGE"
fi

#if(( $(docker ps --filter name=$CONTAINER_NAME --filter status=running  --filter status=restarting | wc -l) == 1))w
#then
#    echo "So Bad = ( "
#    exit 1
#fi
