#!/bin/bash
. shell-config

set_hostname()
{
    local FQDN="$(echo -n "$1" | tr '[[:upper:]]' '[[:lower:]]')"
    if [ -n "$FQDN" ]; then
        shell_config_set "/etc/sysconfig/network" "HOSTNAME" "$FQDN"
        [ -f "/etc/hostname" ] &&  echo "$FQDN" > "/etc/hostname"
    fi
}

set_hostname "$1"
