<h1>Readme</h1>

<h2>Build image with package <u>task-samba-dc-mitkrb5</u></h2>

```bash
$ docker build --build-arg PACKAGE="Default" -t samba-dc-mit-core .
```

For prepare for addition to registry push(example):

```bash
$ docker build --build-arg PACKAGE="Default" \
-t 192.168.200.2/basealt/samba-core-dc-mitkrb5:latest \
-t 192.168.200.2:5000/basealt/samba-core-dc-mitkrb5:0.1 .
```

Push image(example for internal registry)

```bash
$ docker image push 192.168.200.2:5000/basealt/samba-core-dc-mitkrb5:0.1
```


<h2>Build image with package <u>task-samba-dc</u></h2>

```bash
$ docker build --build-arg PACKAGE="Default" -t samba-dc-core .
```

For prepare for addition to registry push(example):

```bash
$ docker build --build-arg PACKAGE="Default" \
-t 192.168.200.2/basealt/samba-core-dc:latest \
-t 192.168.200.2:5000/basealt/samba-core-dc:0.1 .
```

Push image(example for internal registry)

```bash
$ docker image push 192.168.200.2:5000/basealt/samba-core-dc:0.1
```



<h2>Start work</h2>

:zap: Important :zap:
You have to stop host samba service before starting samba-container core

```bash
 $ service samba stop
```

The purpose of this image is to move host' samba server into container.

```bash
$ docker run  --net=host --cap-add=SYS_ADMIN --name=alt_samba_container \
 -v /etc/samba/:/etc/samba/ \
 -v /var/lib/samba/:/var/lib/samba \
 -v /var/cache/samba/:/var/cache/samba \
 samba-dc-mit-core
```

<h3>Work with ```samba-container``` tool</h3>

To use the samba kernel inside container with default parameters, execute next
```bash 
$ samba-container create
$ samba-container provision --realm=domain.alt --domain domain --adminpass='Pa$W0r#dt' --dns-backend=SAMBA_INTERNAL --server-role=dc
$ samba-container start
```
To remove samba core in container with all images and containers, execute
```bash
$ samba-container delete
```
Do not forget that the files created during the work remain in the samba working directories.
Working directories are defined in ```etc/samba/samba_docker.conf```